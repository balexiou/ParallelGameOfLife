#include "mpi.h"
#include "../gameOfLife/gameOfLife.h"
#include "mpiFuncts.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define MAXROUNDS 100
#define CHECKEVERY 10
#define ENABLECHECK 0
#define WRITESTARTTOFILE 0
#define WRITEENDTOFILE 0

int main(int argc, char* argv[])
{
	int** currBlock;
	int** prevBlock;
	int** blocks[2];
	
	int blockRows;
	int blockColumns;
	int totalRows=3000;
	int totalColumns=3000;
	
	int blocksEqual;
	int blocksEqualOverall = 0;
	int round;
	int i;
	int curr,prev;
	int recvIndex;
	int myRank;
	int numOfProcs;
	MPI_Comm comm;
	MPI_Datatype COLUMN;
	MPI_Status recvStatus;
	MPI_Status sendStatus;
	MPI_Request sendReq[2][8];
	MPI_Request recvReq[2][8];
	int cartSize;
	int neighbors[8];
	double localTime,overallTime;
	char *fileName = NULL;
	
	if(argc == 3)
	{
		totalRows = atoi(argv[1]);
		totalColumns = atoi(argv[2]);
	}
	else if(argc == 4)
	{
		totalRows = atoi(argv[1]);
		totalColumns = atoi(argv[2]);
		fileName = argv[3];
	}
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&numOfProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	
	cartSize = sqrt(numOfProcs);
	
	//Making sure the number of processes given is a square
	if(cartSize * cartSize != numOfProcs)
	{
		printf("Number of processes should have an integer square root.\n");
        MPI_Finalize();
        return 1;
	}
	
	if(totalRows%cartSize != 0 || totalColumns%cartSize !=0)
    {
        printf("Number of rows and columns should be able to be perfectly divided with the size of the topology.\n");
        MPI_Finalize();
        return 1;
    }
    blockRows = totalRows/cartSize;
    blockColumns = totalColumns/cartSize;

	createCart(&comm,cartSize);		//creating the cartesian topology
	
	MPI_Comm_rank(comm, &myRank);
	srand(myRank);
	//stride is blockColumns +2 because we will increase the size of each block by 2 to have space to save the neighboring nodes
	MPI_Type_vector(blockRows, 1, blockColumns +2, MPI_INT, &COLUMN); 
    MPI_Type_commit(&COLUMN);
	
	findNeighbors(comm,neighbors);
	printf("proccess %d with neighbors %d %d %d %d %d %d %d %d\n",myRank,neighbors[0],neighbors[1],neighbors[2],neighbors[3],neighbors[4],neighbors[5],neighbors[6],neighbors[7]);
	
	blockRows+=2;
	blockColumns+=2;
	
	currBlock= allocateBlock(blockRows,blockColumns);
	prevBlock= allocateBlock(blockRows,blockColumns);
	
	if(fileName == NULL)
	{
		initBlock(currBlock,blockRows,blockColumns);
		#if(WRITESTARTTOFILE)
			char fileName2[11] ="startState";
			writeToFile(comm, currBlock, fileName2, myRank , blockRows ,blockColumns , totalRows, totalColumns);
		#endif
	}
	else
	{
		readFromFile(comm, currBlock, fileName, myRank , blockRows ,blockColumns , totalRows, totalColumns);
	}
	
	blocks[0]= currBlock;
	blocks[1]= prevBlock;
	
	initPersistentSendCon(comm,blocks[0], blockRows ,blockColumns , neighbors, sendReq[0],COLUMN);
	initPersistentSendCon(comm,blocks[1], blockRows ,blockColumns , neighbors, sendReq[1],COLUMN);
	initPersistentRecvCon(comm,blocks[0], blockRows ,blockColumns , neighbors, recvReq[0],COLUMN);
	initPersistentRecvCon(comm,blocks[1], blockRows ,blockColumns , neighbors, recvReq[1],COLUMN);
	
	MPI_Barrier(comm);
	
	localTime = MPI_Wtime();
	
	for(round=1; round<= MAXROUNDS ; round++)
	{
		curr = round%2;
		prev = (round + 1)%2;
		for(i=0;i<8;i++)
			MPI_Start(&sendReq[prev][i]);
			
		for(i=0;i<8;i++)
			MPI_Start(&recvReq[prev][i]);
		
		updateInnerBlock( blocks[prev], blocks[curr], blockRows , blockColumns);
		
		for(i=0;i<8;i++)
		{
			MPI_Waitany(8,recvReq[prev],&recvIndex,&recvStatus);
			if(recvIndex <= RIGHT)
				updateBorder(blocks[prev], blocks[curr], blockRows, blockColumns, recvIndex);
		}
		for(i=LEFTUP;i<=RIGHTDOWN;i++)
			updateBorder(blocks[prev], blocks[curr], blockRows, blockColumns, i);
		
		#if(ENABLECHECK)
		if(round % CHECKEVERY == 0)
		{
			blocksEqual = blocksAreEqual(blocks[prev],blocks[curr],blockRows,blockColumns);
			//printf("round %d proc %d equal %d\n",round,myRank,blocksEqual);
			//printf("-------------1------------------\n");
			//printBlock(blocks[prev],blockRows,blockColumns);
			//printf("-------------2------------------\n");
			//printBlock(blocks[curr],blockRows,blockColumns);
			MPI_Allreduce(&blocksEqual, &blocksEqualOverall, 1, MPI_INT, MPI_BAND, comm);
			if(blocksEqualOverall == 1)
				break;
		}
		#endif
		for(i=0; i <8; i++) 
        {
            MPI_Wait(&sendReq[prev][i], &sendStatus);
        }
        //if(myRank==0)
			//printBlock(blocks[prev],blockRows,blockColumns);
	}
	
	localTime = MPI_Wtime() - localTime;
	MPI_Reduce(&localTime, &overallTime, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
	#if(WRITEENDTOFILE)
		char fileName3[10] ="endState";
		writeToFile(comm, blocks[curr], fileName3, myRank , blockRows  ,blockColumns, totalRows, totalColumns);
	#endif
	if(myRank==0)
	{
		printf("The game ended after %d rounds and %f seconds.\n",round - 1 , overallTime);
	}
	
	freeBlock(currBlock);
	freeBlock(prevBlock);
	MPI_Comm_free(&comm);
    MPI_Type_free(&COLUMN);
	MPI_Finalize();
	return 0;
}
