#ifndef MPIFUNCTS_H
#define MPIFUNCTS_H

void createCart(MPI_Comm* comm, int cartSize);
void findNeighbors(MPI_Comm communicator, int* neighbors);
void initPersistentSendCon(MPI_Comm comm, int** block , int numOfRows,int numOfColumns, int* neighbors, MPI_Request* sendReq, MPI_Datatype COLUMN);
void initPersistentRecvCon(MPI_Comm comm, int** block , int numOfRows,int numOfColumns, int* neighbors, MPI_Request* recvReq, MPI_Datatype COLUMN);
void readFromFile( MPI_Comm comm, int** block, char* fileName, int myRank, int blockRows, int blockColumns, int totalRows, int totalColumns);
void writeToFile( MPI_Comm comm, int** block, char* fileName, int myRank, int blockRows, int blockColumns, int totalRows, int totalColumns);
#endif
