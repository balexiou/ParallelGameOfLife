//
// Created by Babis on 9/16/2017.
//

#ifndef GOL_H
#define GOL_H

enum Neighbors
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    LEFTUP,
    RIGHTUP,
    LEFTDOWN,
    RIGHTDOWN,
};

int ** allocateBlock(int numOfRows,int numOfColumns);
void freeBlock(int **block);
void initBlock(int ** block , int numOfRows, int numOfColumns);
void updateInnerBlock(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns);
void updateBorder(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns,int whichPart);
int numOfActiveNeighbors(int **oldBlock, int xCoord, int yCoord, int nRows, int nColumns);
int blocksAreEqual(int **oldBlock, int** newBlock, int numOfRows, int numOfColumns);
void printBlock(int **oldBlock, int nRows, int nColumns);
int cellUpdate(int old_cell_value, int number_of_active_neighbors);

#endif //GOL_H