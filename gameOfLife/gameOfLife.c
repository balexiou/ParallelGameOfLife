#include "gameOfLife.h"
#include <stdlib.h>
#include <stdio.h>
#define POPULATIONVARIABLE 5

//Allocates a continuous block of data
int ** allocateBlock(int numOfRows,int numOfColumns)
{
	int i;
    int *data;
    int ** block;
 
    block = malloc(numOfRows * sizeof (int*)); /* Allocating pointers */
    data = malloc(numOfRows * numOfColumns * sizeof (int)); /* Allocating data */
    for (i = 0; i < numOfRows; i++)
        block[i] = data + i * (numOfColumns);
 
    return block;
}

void freeBlock(int **block)
{
	free(block[0]);
	free(block);
}

//initializes the block , density depends on POPULATIONVARIABLE
void initBlock(int ** block , int numOfRows, int numOfColumns)
{
	int i,j;
	for(i=1; i < numOfRows -1 ;i++)
		for(j=1;j < numOfColumns - 1 ; j++)
		{
			if( rand()%POPULATIONVARIABLE == 0)
				block[i][j] = 1;
			else
				block[i][j] = 0;
		}
}

void updateValue(int** oldBlock, int** newBlock,int i, int j)
{
	int numOfNeighbors = numOfActiveNeighbors(oldBlock,i,j);
	if(oldBlock[i][j] == 0 && numOfNeighbors ==3)
		newBlock[i][j] = 1;
	else if(oldBlock[i][j] == 1 &&(numOfNeighbors ==2 || numOfNeighbors==3))
		newBlock[i][j] = 1;
	else
		newBlock[i][j] = 0;
}

void updateInnerBlock(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns)
{
	int i,j;
	for(i=2; i < numOfRows -2 ;i++)
		for(j=2;j < numOfColumns - 2 ; j++)
			updateValue(oldBlock , newBlock , i , j );
}

void updateBorder(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns,int whichPart)
{
	int i,j;
	if(whichPart == UP)
	{
		i = 1;
		for(j = 2;j < numOfColumns -2; j++)
			updateValue(oldBlock , newBlock , i , j );
	}
	else if(whichPart == DOWN)
	{
		i = numOfRows - 2;
		for(j = 2;j < numOfColumns -2; j++)
			updateValue(oldBlock , newBlock , i , j);
	}
	else if(whichPart == LEFT)
	{
		j = 1;
		for(i = 2;i < numOfRows -2;i++)
			updateValue(oldBlock , newBlock , i , j);
	}
	else if(whichPart == RIGHT)
	{
		j = numOfColumns -2;
		for(i = 2;i < numOfRows -2;i++)
			updateValue(oldBlock , newBlock , i , j);
	}
	else if(whichPart == LEFTUP)
	{
		updateValue(oldBlock , newBlock , 1 , 1 );
	}
	else if(whichPart == RIGHTUP)
	{
		updateValue(oldBlock , newBlock , 1 , numOfColumns -2 );
	}
	else if(whichPart == LEFTDOWN)
	{
		updateValue(oldBlock , newBlock , numOfRows - 2 , 1 );
	}
	else if(whichPart == RIGHTDOWN)
	{
		updateValue(oldBlock , newBlock , numOfRows - 2 , numOfColumns -2 );
	}
}

int numOfActiveNeighbors(int** oldBlock ,int x, int y )
{
	int activeNeighbors;
	
	activeNeighbors = oldBlock[x-1][y-1] + oldBlock[x-1][y] + oldBlock[x-1][y+1] + 
					oldBlock[x][y-1] + oldBlock[x][y+1] +
					oldBlock[x+1][y-1] + oldBlock[x+1][y] + oldBlock[x+1][y+1];
	return activeNeighbors;
}

//returns 1 if the 2 blocks are equal , 0 otherwise
int blocksAreEqual(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns)
{
	int i,j;
	for(i=1;i < numOfRows -1 ;i++)
		for(j=1;j < numOfColumns -1 ;j++)
			if(oldBlock[i][j] != newBlock[i][j])
				return 0;
	return 1;
}

//Used for debugging
void printBlock(int** block, int numOfRows, int numOfColumns)
{
    int i, j;
    for(i = 0; i < numOfRows; i++)
    {
        for(j =0; j < numOfColumns; j++)
			if(block[i][j] == 1)			
				printf(" * ");
			else
				printf("   ");
        printf("\n");
    }
    printf("---------------------------------\n");
}
