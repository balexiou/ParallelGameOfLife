#include "mpi.h"
#include <stdlib.h>
#include "../gameOfLife/gameOfLife.h"
#include "mpiFuncts.h"


void createCart(MPI_Comm* comm, int cartSize)
{
    int numOfDimensions, reorder;
	int dims[2]={cartSize,cartSize}; //number of processes in each dimension
	int periods[2]={1,1}; //1 since we have a periodic topology

    numOfDimensions = 2; //dimensions of the topology
	reorder = 1; //doesn't matter currently according to documentation

    MPI_Cart_create(MPI_COMM_WORLD, numOfDimensions, dims, periods, reorder, comm);
}

//Fills the neighbors matrix with the ranks for the neighbors
void findNeighbors(MPI_Comm communicator, int* neighbors)
{
	int upRank, downRank, rightRank, leftRank;
	int upLeftRank,upRightRank,downLeftRank,downRightRank;
	int procCoords[2];
	MPI_Cart_shift(communicator, 1, -1, &rightRank, &leftRank); //with one call finds both neighbors of each dimension 
	MPI_Cart_shift(communicator, 0, -1, &downRank, &upRank);
	
	MPI_Cart_coords(communicator, upRank, 2, procCoords);   //using the top neighbor finds topLeft and topRight
	procCoords[1]--;
	MPI_Cart_rank(communicator, procCoords, &upLeftRank);
	procCoords[1]+=2;
	MPI_Cart_rank(communicator, procCoords, &upRightRank);
	
	MPI_Cart_coords(communicator, downRank, 2, procCoords);  //using the bottom neighbor finds bottomLeft and bottomRight
	procCoords[1]--;
	MPI_Cart_rank(communicator, procCoords, &downLeftRank);
	procCoords[1]+=2;
	MPI_Cart_rank(communicator, procCoords, &downRightRank);
	
	neighbors[0]= upRank;
	neighbors[1]= downRank;
	neighbors[2]= leftRank;
	neighbors[3]= rightRank;
	neighbors[4]= upLeftRank;
	neighbors[5]= upRightRank;
	neighbors[6]= downLeftRank;
	neighbors[7]= downRightRank;
}

//Initialize the persistent send requests for one block
void initPersistentSendCon(MPI_Comm comm, int** block , int numOfRows,int numOfColumns, int* neighbors, MPI_Request* sendReq, MPI_Datatype COLUMN)
{
	MPI_Send_init(&block[1][1], numOfColumns - 2, MPI_INT, neighbors[0], DOWN, comm, &sendReq[0]);
	MPI_Send_init(&block[numOfRows-2][1], numOfColumns - 2, MPI_INT, neighbors[1], UP, comm, &sendReq[1]);
	MPI_Send_init(&block[1][1], 1, COLUMN, neighbors[2], RIGHT, comm, &sendReq[2]);
	MPI_Send_init(&block[1][numOfColumns-2], 1, COLUMN, neighbors[3], LEFT, comm, &sendReq[3]);
	MPI_Send_init(&block[1][1], 1, MPI_INT, neighbors[4], RIGHTDOWN, comm, &sendReq[4]);
	MPI_Send_init(&block[1][numOfColumns-2], 1, MPI_INT, neighbors[5], LEFTDOWN, comm, &sendReq[5]);
	MPI_Send_init(&block[numOfRows-2][1], 1, MPI_INT, neighbors[6], RIGHTUP, comm, &sendReq[6]);
	MPI_Send_init(&block[numOfRows-2][numOfColumns-2], 1, MPI_INT, neighbors[7], LEFTUP, comm, &sendReq[7]);
}

//Initialize the persistent receive requests for one block
void initPersistentRecvCon(MPI_Comm comm, int** block , int numOfRows,int numOfColumns, int* neighbors, MPI_Request* recvReq, MPI_Datatype COLUMN)
{
	MPI_Recv_init(&block[0][1], numOfColumns - 2, MPI_INT, neighbors[0], UP, comm, &recvReq[0]);
	MPI_Recv_init(&block[numOfRows-1][1], numOfColumns - 2, MPI_INT, neighbors[1], DOWN, comm, &recvReq[1]);
	MPI_Recv_init(&block[1][0], 1, COLUMN, neighbors[2], LEFT, comm, &recvReq[2]);
	MPI_Recv_init(&block[1][numOfColumns-1], 1, COLUMN, neighbors[3], RIGHT, comm, &recvReq[3]);
	MPI_Recv_init(&block[0][0], 1, MPI_INT, neighbors[4], LEFTUP, comm, &recvReq[4]);
	MPI_Recv_init(&block[0][numOfColumns-1], 1, MPI_INT, neighbors[5], RIGHTUP, comm, &recvReq[5]);
	MPI_Recv_init(&block[numOfRows-1][0], 1, MPI_INT, neighbors[6], LEFTDOWN, comm, &recvReq[6]);
	MPI_Recv_init(&block[numOfRows-1][numOfColumns-1], 1, MPI_INT, neighbors[7], RIGHTDOWN, comm, &recvReq[7]);
}

void readFromFile( MPI_Comm comm, int** block, char* fileName, int myRank, int blockRows, int blockColumns, int totalRows, int totalColumns)
{
    MPI_File file;
    MPI_Datatype NEWTYPE;
    int totalDimensions[2] = {totalRows , totalColumns};
    int blockDimensions[2] = {blockRows -2 , blockColumns - 2};
    int startArr[2];
    int procCoords[2];
    int i;

	MPI_Cart_coords(comm, myRank, 2, procCoords); 	//getting my coords
	
    startArr[0] = procCoords[0] * (blockRows - 2);		//Will read from the file according to the coords
    startArr[1] = procCoords[1] * (blockColumns - 2);
    
    MPI_File_open(comm, fileName, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);

    MPI_Type_create_subarray(2, totalDimensions, blockDimensions, startArr, MPI_ORDER_C, MPI_INT, &NEWTYPE);
    MPI_Type_commit(&NEWTYPE);

    MPI_File_set_view(file, 0, MPI_INT, NEWTYPE, "native", MPI_INFO_NULL);

    for(i =1; i < blockRows-1; i++)
    {
        MPI_File_read_all(file, &block[i][1], blockColumns - 2, MPI_INT, MPI_STATUS_IGNORE);
    }
    
    MPI_File_close(&file);
    MPI_Type_free(&NEWTYPE);
}

void writeToFile( MPI_Comm comm, int** block, char* fileName, int myRank, int blockRows, int blockColumns, int totalRows, int totalColumns)
{
	//printf("Write to file>?.\n");
    MPI_File file;
    MPI_Datatype NEWTYPE;
    int totalDimensions[2] = {totalRows , totalColumns};
    int blockDimensions[2] = {blockRows -2 , blockColumns - 2};
    int startArr[2];
    int procCoords[2];
    int i;

	MPI_Cart_coords(comm, myRank, 2, procCoords); //getting my coords
	
    startArr[0] = procCoords[0] * (blockRows - 2);		//Will read from the file according to the coords
    startArr[1] = procCoords[1] * (blockColumns - 2);
    
    MPI_File_open(comm, fileName, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file); //creates file if it doesn't exist

    MPI_Type_create_subarray(2, totalDimensions, blockDimensions, startArr, MPI_ORDER_C, MPI_INT, &NEWTYPE);  //Subarray using starArr to write in the correct place
    MPI_Type_commit(&NEWTYPE);

    MPI_File_set_view(file, 0, MPI_INT, NEWTYPE, "native", MPI_INFO_NULL);

    for(i =1; i < blockRows-1; i++)
    {
        MPI_File_write_all(file, &block[i][1], blockColumns - 2, MPI_INT, MPI_STATUS_IGNORE);
    }
    
    MPI_File_close(&file);
    MPI_Type_free(&NEWTYPE);
}
