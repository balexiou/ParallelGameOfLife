#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "GoL.h"

#define MAX_LOOPS 100

int main(int argc, char **argv) {

    int **previous_block;
    int **current_block;
    int **temporary_block;

    int i, j, k;
    int number_of_rows = atoi(argv[1]);
    int number_of_columns = atoi(argv[2]);
    
    int stalemate= 0;

    clock_t start_time, finished_time;
    double elapsed_time = 0.0;

    if (argc != 3){
        printf("Usage: %s -r[Number of Rows] -c[Number of Columns]\n", argv[0]);
        exit(1);
    }

    if (number_of_rows <= 0 || number_of_columns <= 0) {
        printf("Invalid rows or columns number! [Value must be > 0]\n");
        exit(2);
    }


    /* Allocating Memory for our Blocks */
    previous_block = allocateBlock(number_of_rows, number_of_columns);
    current_block = allocateBlock(number_of_rows, number_of_columns);
    temporary_block = allocateBlock(number_of_rows, number_of_columns);

    /* Initialize Block */
    initBlock(previous_block, number_of_rows, number_of_columns);

    /* Printing Initial Block */
    //printBlock(previous_block, number_of_rows, number_of_columns);

    /* Nested Loops to serially update each cell's value */
    start_time = clock();
    for (int l = 0; l < 10; ++l) {
        start_time = clock();
        for (k = 0; k < MAX_LOOPS; ++k){
            for (i = 0; i < number_of_rows; ++i) {
                for (j = 0; j < number_of_columns; ++j) {
                    current_block[i][j] = cellUpdate(previous_block[i][j], numOfActiveNeighbors(previous_block,i,j,number_of_rows,number_of_columns));
                }
            }

            /* Printing Updated Block*/
            //printBlock(current_block, number_of_rows, number_of_columns);

            if (blocksAreEqual(previous_block, current_block, number_of_rows, number_of_columns)){
                stalemate += stalemate;
                if (stalemate >= 10){
                    printf("No changes for 10 consecutive rounds! Aborting execution...\n");
                    break;
                }
                else {
                    stalemate = 0;
                }
            }

            /* Swapping Blocks */
            temporary_block = previous_block;
            previous_block = current_block;
            current_block = temporary_block;
        }
        finished_time = clock();

        elapsed_time += (double)(finished_time - start_time) / CLOCKS_PER_SEC;
    }
    elapsed_time = elapsed_time / 10;
    printf("Elapsed time : %.6f\n", elapsed_time);

    /* Freeing Allocated Memory */
    freeBlock(current_block);
    freeBlock(previous_block);

    return 0;
}
