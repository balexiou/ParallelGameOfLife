//
// Created by Babis on 9/16/2017.
//

#include <stdlib.h>
#include <stdio.h>

#include "GoL.h"
#define POPULATIONVARIABLE 5

/* Allocates a continuous block of data */
int ** allocateBlock(int numOfRows,int numOfColumns)
{
    int i;
    int *data;
    int ** block;

    block = malloc(numOfRows * sizeof (int*)); /* Allocating pointers */
    data = malloc(numOfRows * numOfColumns * sizeof (int)); /* Allocating data */
    for (i = 0; i < numOfRows; i++)
        block[i] = data + i * (numOfColumns);

    return block;
}

void freeBlock(int **block)
{
    free(block[0]);
    free(block);
}

/* Initializes the block , density depends on POPULATIONVARIABLE */
void initBlock(int ** block , int numOfRows, int numOfColumns)
{
    int i,j;
    for(i=1; i < numOfRows -1 ;i++)
        for(j=1;j < numOfColumns - 1 ; j++) {
            if (rand() % POPULATIONVARIABLE == 0)
                block[i][j] = 1;
            else
                block[i][j] = 0;
        }
}

int numOfActiveNeighbors(int **oldBlock, int x, int y, int number_of_rows, int number_of_columns)
{
    int top_row_x, left_column_y, bottom_row_x, right_column_y;
    int number_of_active_neighbors;

    if(x == 0 || x == number_of_rows-1 || y == 0 || y == number_of_columns-1){
        top_row_x = (x-1+number_of_rows)%number_of_rows;
        left_column_y = (y-1 + number_of_columns)%number_of_columns;

        bottom_row_x = (x+1)%number_of_rows;
        right_column_y = (y+1)%number_of_columns;

        number_of_active_neighbors =  oldBlock[top_row_x][left_column_y]+ oldBlock[top_row_x][y]+oldBlock[top_row_x][right_column_y] //the up 3 neighbors
                                      +oldBlock[x][left_column_y]+oldBlock[x] [right_column_y] //the middle neighbors
                                      +oldBlock[bottom_row_x][left_column_y]+ oldBlock[bottom_row_x][y]+oldBlock[bottom_row_x][right_column_y]; //the down 3 neighbors
        return number_of_active_neighbors;
    }
    else {
        number_of_active_neighbors = oldBlock[x-1][y-1] + oldBlock[x-1][y] + oldBlock[x-1][y+1] +
                          oldBlock[x][y-1] + oldBlock[x][y+1] +
                          oldBlock[x+1][y-1] + oldBlock[x+1][y] + oldBlock[x+1][y+1];
        return number_of_active_neighbors;
    }

}

/* Returns 1 if the 2 blocks are equal , 0 otherwise */
int blocksAreEqual(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns)
{
    int i,j;
    for(i=1;i < numOfRows -1 ;i++)
        for(j=1;j < numOfColumns -1 ;j++)
            if(oldBlock[i][j] != newBlock[i][j])
                return 0;
    return 1;
}

/* Used for debugging */
void printBlock(int** block, int numOfRows, int numOfColumns)
{
    int i, j;
    for(i = 0; i < numOfRows; i++)
    {
        for(j =0; j < numOfColumns; j++)
            if(block[i][j] == 1)
                printf(" * ");
            else
                printf("   ");
        printf("\n");
    }
    printf("\t\t\t\t ----------- END OF ROUND ----------- \n");
}

/* Updates the cell value depending on its neighbours state */
int cellUpdate(int old_cell_value, int number_of_active_neighbors){
    if (old_cell_value == 1){
        if (number_of_active_neighbors == 2 || number_of_active_neighbors == 3)
            return 1;
        else
            return 0;
    }
    else {
        if (number_of_active_neighbors == 3)
            return 1;
        else
            return 0;
    }
}