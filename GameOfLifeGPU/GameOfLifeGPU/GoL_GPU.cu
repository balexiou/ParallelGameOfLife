
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LOOPS 100
#define POPULATIONVARIABLE 5

cudaError_t GoL_GPU(int **h_old_block, int **h_current_block, int number_of_rows, int number_of_columns);
int ** allocateBlock(int numOfRows, int numOfColumns);
void freeBlock(int **block);
void initBlock(int ** block, int numOfRows, int numOfColumns);

__global__ void updateCellValue_GPU(int *old_block, int *current_block, int number_of_rows, int number_of_columns)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

	int x;
	int y;
	int top_row_x;					/* Top matrix row on the x axis */
	int leftmost_column_y;			/* Leftmost matrix column on the y axis */
	int bottom_row_x;				/* Last matrix row on the x axis*/
	int rightmost_column_y;			/* Rightmost matrix column on the y axis */
	int number_of_active_neighbors;

	top_row_x = number_of_columns * ((x - 1 + number_of_rows) % number_of_rows);
	leftmost_column_y = (y - 1 + number_of_columns) % number_of_columns;

	bottom_row_x = number_of_columns * ((x + 1) % number_of_rows);
	rightmost_column_y = (y + 1) % number_of_columns;

	number_of_active_neighbors = old_block[top_row_x + leftmost_column_y] + old_block[top_row_x + y] + old_block[top_row_x + rightmost_column_y]
		+ old_block[x*number_of_columns + leftmost_column_y] + old_block[x*number_of_columns + rightmost_column_y]
		+ old_block[bottom_row_x + leftmost_column_y] + old_block[bottom_row_x + y] + old_block[bottom_row_x + rightmost_column_y];

	if (number_of_active_neighbors == 3 || (number_of_active_neighbors == 2 && old_block[(x * number_of_columns) + y] == 1))
	{
		current_block[(x * number_of_columns) + y] = 1;
	}
	else
	{
		current_block[(x * number_of_columns) + y] = 0;
	}
}

int main()
{
	int **h_old_block;
	int **h_current_block;

	int number_of_rows = 7200;
	int number_of_columns = 7200;

	h_old_block = allocateBlock(number_of_rows, number_of_columns);
	h_current_block = allocateBlock(number_of_rows, number_of_columns);

	initBlock(h_old_block, number_of_rows, number_of_columns);

	GoL_GPU(h_old_block, h_current_block, number_of_rows, number_of_columns);


	free(h_old_block);
	free(h_current_block);

	return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t GoL_GPU(int **h_old_block, int **h_current_block, int number_of_rows, int number_of_columns)
{
	cudaError_t cudaStatus;

	int *d_old_block;
	int *d_current_block;
	int *d_temporary_block;

	clock_t start_time;
	clock_t finish_time;

	double elapsed = 0.0;

	int memory_size;

	int N = number_of_columns * number_of_rows;

	const int THREADS_PER_BLOCK = 1024;
	const int BLOCK_SIZE = (N + THREADS_PER_BLOCK - 1 )/ THREADS_PER_BLOCK;

	memory_size = N * sizeof(int);

	cudaStatus = cudaMalloc((void **)&d_old_block, memory_size);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed! [d_old_block]\n");
		goto Error;
	}

	cudaStatus = cudaMalloc((void **)&d_current_block, memory_size);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed! [d_current_block]\n");
		goto Error;
	}

	cudaStatus = cudaMemcpy(d_old_block, &h_old_block[0][0], memory_size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed! [cudaMemcpyHostToDevice]\n");
		goto Error;
	}

	start_time = clock();
	for (int j = 0; j < MAX_LOOPS; j++)
	{
		updateCellValue_GPU << < BLOCK_SIZE, THREADS_PER_BLOCK >> >(d_old_block, d_current_block, number_of_rows, number_of_columns);

		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "updateCellValue_GPU launch failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}

		d_temporary_block = d_old_block;
		d_old_block = d_current_block;
		d_current_block = d_temporary_block;

	}

	cudaStatus = cudaDeviceSynchronize();
	finish_time = clock();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching updateCellValue_GPU!\n", cudaStatus);
		goto Error;
	}

	elapsed = (double)(finish_time - start_time) / CLOCKS_PER_SEC;
	printf("Elapsed time :%.8f\n", elapsed);

	cudaStatus = cudaMemcpy(&h_current_block[0][0], d_old_block, memory_size, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed! [cudaMemcpyDeviceToHost]\n");
		goto Error;
	}

	cudaFree(d_old_block);
	cudaFree(d_current_block);

Error:
	cudaFree(d_old_block);
	cudaFree(d_current_block);

	return cudaStatus;
}

int ** allocateBlock(int numOfRows, int numOfColumns)
{
	int i;
	int *data;
	int ** block;

	block = (int **)malloc(numOfRows * sizeof(int*)); /* Allocating pointers */
	data = (int *)malloc(numOfRows * numOfColumns * sizeof(int)); /* Allocating data */
	for (i = 0; i < numOfRows; i++)
		block[i] = data + i * (numOfColumns);

	return block;
}

void freeBlock(int **block)
{
	free(block[0]);
	free(block);
}
void initBlock(int ** block, int numOfRows, int numOfColumns)
{
	int i, j;
	for (i = 1; i < numOfRows - 1; i++)
		for (j = 1; j < numOfColumns - 1; j++) {
			if (rand() % POPULATIONVARIABLE == 0)
				block[i][j] = 1;
			else
				block[i][j] = 0;
		}
}
