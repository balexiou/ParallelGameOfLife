#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

enum Neighbors
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    LEFTUP,
    RIGHTUP,
    LEFTDOWN,
    RIGHTDOWN,
};

int ** allocateBlock(int numOfRows,int numOfColumns);
void freeBlock(int **block);
void initBlock(int ** block , int numOfRows, int numOfColumns);
void updateValue(int** oldBlock, int** newBlock,int i, int j);
void updateInnerBlock(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns);
void updateBorder(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns,int whichPart);
int numOfActiveNeighbors(int** oldBlock ,int x, int y);
int blocksAreEqual(int** oldBlock, int** newBlock, int numOfRows, int numOfColumns);
void printBlock(int** block, int nRows, int nColumns);
#endif
